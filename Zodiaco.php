<?php
do {
    do {
        echo "In che mese sei nato? [1-12]\nMese: ";
        $handle = fopen("php://stdin", "r");
        $line = fgets($handle);
        $mese = (int)$line;
    } while ($mese < 1 || $mese > 12);

    do {
        echo "Che giorno sei nato? [1-31]\nGiorno: ";
        $handle2 = fopen("php://stdin", "r");
        $line2 = fgets($handle);
        $giorno = (int)$line2;
    } while ($giorno < 1 || $giorno > 31);
}while(!isValido());

echo "Sei nato il ".$giorno."/".$mese."!\n";

calcoloSegno();

/*do {
    echo "A che ora sei nato? [0000 - 2359]\nOra: ";
    $handle = fopen("php://stdin", "r");
    $line = fgets($handle);
    $ora = (int)$line;
    echo $ora."\n";
    $ore = substr($ora, 0, 2);
    $minuti = substr($ora, 2, 2);
} while (false); */

echo $ore.":".$minuti;


function isValido(){
    GLOBAL $giorno;
    GLOBAL $mese;

    if($mese == [1,3,5,7,8,10,12]) {
        echo "Giorno non valido\n";
        return true;
    }

    if($giorno == 31) {
        echo "Giorno non valido\n";
        return false;
    }

    if($giorno > 29 && $mese == 2) {
        echo "Giorno non valido\n";
        return false;
    }

    return true;
}


function calcoloSegno() {
    GLOBAL $mese;
    GLOBAL $giorno;
    $var = $mese * 100 + $giorno;
    if($var >= 321 && $var <= 420) {
        echo "Sei dell'ariete!\n";
        return 0;
    }
    if($var >= 421 && $var <= 520) {
        echo "Sei del toro!\n";
        return 0;
    }
    if($var >= 521 && $var <= 621) {
        echo "Sei dei gemelli!\n";
        return 0;
    }
    if($var >= 622 && $var <= 722) {
        echo "Sei del cancro!\n";
        return 0;
    }
    if($var >= 723 && $var <= 823) {
        echo "Sei del leone!\n";
        return 0;
    }
    if($var >= 824 && $var <= 922) {
        echo "Sei della vergine!\n";
        return 0;
    }
    if($var >= 923 && $var <= 1022) {
        echo "Sei della bilancia!\n";
        return 0;
    }
    if($var >= 1023 && $var <= 1122) {
        echo "Sei dello scorpione!\n";
        return 0;
    }
    if($var >= 1123 && $var <= 1221) {
        echo "Sei del sagittario!\n";
        return 0;
    }
    if($var >= 1222 || $var <= 120) {
        echo "Sei del capricorno!\n";
        return 0;
    }
    if($var >= 121 && $var <= 219) {
        echo "Sei dell'acquario!\n";
        return 0;
    }
    if($var >= 221 && $var <= 320) {
        echo "Sei dei pesci!\n";
        return 0;
    }
}




?>